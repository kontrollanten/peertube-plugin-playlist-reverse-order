async function register ({
  peertubeHelpers,
  registerHook,
  registerSetting,
  settingsManager,
}) {
  const { database, logger } = peertubeHelpers

  settingsManager.onSettingsChange((settings) => {
    if (settings['remove-deleted-videos-from-playlists']) {
      handlePurgePlaylists({ database, logger })
    }
  })

  registerHook({
    target: 'action:api.video-playlist-element.created',
    handler: async ({ playlistElement }) => {
      await handlePlaylistElementAdded({
        database,
        getSetting: settingsManager.getSetting,
        logger,
        playlistElement,
      })
    },
  })

  registerHook({
    target: 'action:api.video.updated',
    handler: async ({ video }) => {
      await handleVideoUpdated({
        database,
        getSetting: settingsManager.getSetting,
        logger,
        video,
      })
    },
  })

  registerHook({
    target: 'action:api.video.deleted',
    handler: async ({ video }) => {
      await handleVideoDeleted({
        database,
        getSetting: settingsManager.getSetting,
        logger,
        video,
      })
    },
  })

  registerHook({
    target: 'filter:api.video-playlist.videos.list.result',
    handler: (result, params) => {
      return handleVideoPlaylistListResult({
        getSetting: settingsManager.getSetting,
        result,
      })
    },
  })

  registerSetting({
    name: 'remove-deleted-videos-from-playlists',
    label: 'Remove deleted videos (for all accounts) in playlists',
    type: 'input-checkbox',
    default: false,
    descriptionHTML: 'Per default deleted videos will remain in the playlists (shown as deleted), by enable this they will be removed from the playlists. This is a global setting for all accounts.',
  })

  registerSetting({
    name: 'hide-private-videos',
    label: 'Hide private videos (for all accounts) in playlists',
    type: 'input-checkbox',
    default: false,
    descriptionHTML: 'Per default private videos will remain in the playlists (shown as private), by enable this they will be hidden. This is a global setting for all accounts.',
  })

  registerSetting({
    name: 'reverse-order-account-id',
    label: 'Account id',
    type: 'input',
    descriptionHTML: 'Reverse ordered playlist will only be for this particular account id.',
  })

  registerSetting({
    name: 'order-by',
    label: 'Order by',
    type: 'select',
    default: 'latest-added',
    options: [
      {
        label: 'Latest added to playlist',
        value: 'latest-added',
      },
      {
        label: 'Publish date',
        value: 'publish-date',
      }
    ],
  })
}

const handlePurgePlaylists = async ({
  database,
  logger,
}) => {
  logger.info('Will purge removed videos from all playlists.')

  const [, metadata] = await database.query('DELETE FROM "videoPlaylistElement" WHERE "videoId" IS NULL')

  logger.info(`${metadata?.rowCount} empty elements deleted from playlists.`)
};

const handlePlaylistElementAdded = async ({
  database,
  getSetting,
  logger,
  playlistElement,
}) => {
  const playlistId = playlistElement.videoPlaylistId

  await updatePlaylistIfNeeded({
    database,
    getSetting,
    logger,
    playlistId,
  })
}

const handleVideoUpdated = async ({
  database,
  getSetting,
  logger,
  video,
}) => {
  const [rows] = await database.query('SELECT "videoPlaylistId" FROM public."videoPlaylistElement" WHERE "videoId" = ?', { replacements: [video.id] });

  rows.forEach(async (row) => {
    updatePlaylistIfNeeded({
      database,
      getSetting,
      logger,
      playlistId: row.videoPlaylistId,
    });
  });
};

const handleVideoDeleted = async ({
  database,
  getSetting,
  logger,
  video,
}) => {
  const removeDeletedVideosFromPlaylists = await getSetting('remove-deleted-videos-from-playlists')

  if (!removeDeletedVideosFromPlaylists) return;

  logger.info(`Will purge playlists since video ${video.id} was deleted.`)

  handlePurgePlaylists({ database, logger })
}

const updatePlaylistIfNeeded = async ({
  database,
  getSetting,
  logger,
  playlistId,
}) => {
  const accountId = await getSetting('reverse-order-account-id')
  const [,check] = await database.query('SELECT id FROM "videoPlaylist" WHERE id = ? AND "ownerAccountId" = ?', { replacements: [playlistId, +accountId] })

  if (check.rows.length === 0) {
    logger.debug('Account owner doesn\'t exist in settings, do nothing and return.')
    return
  }

  const orderBy = await getSetting('order-by')

  let rows;
  switch (orderBy) {
    case 'publish-date':
      [rows] = await database.query(`
        SELECT "videoPlaylistId", "videoPlaylistElement".id FROM "videoPlaylistElement"
          INNER JOIN video ON video.id = "videoPlaylistElement"."videoId"
        WHERE "videoPlaylistElement"."videoPlaylistId" = ?
        ORDER BY CASE
          WHEN "video"."originallyPublishedAt" IS NOT NULL THEN "video"."originallyPublishedAt"
          ELSE "video"."publishedAt"
        END
        DESC`, { replacements: [playlistId] })
      break;
    default:
      [rows] = await database.query('SELECT id FROM "videoPlaylistElement" WHERE "videoPlaylistId" = ? ORDER BY "createdAt" DESC', { replacements: [playlistId] })
  }

  let incr = 1
  for (const row of rows) {
    const res = await database.query('UPDATE "videoPlaylistElement" SET position = ? WHERE "id" = ?', { replacements: [incr, row.id] })
    incr++
  }

  logger.debug(`Updated order of playlist ${playlistId}`)
}

const handleVideoPlaylistListResult = async ({ getSetting, result }) => {
  const hidePrivateVideos = await getSetting('hide-private-videos')

  if (!hidePrivateVideos) return result;

  const filteredData = result.data.filter(e => {
    if (hidePrivateVideos && e.Video && e.Video.dataValues.privacy !== 1) {
      return false
    }

    return true
  })

  return {
    total: result.total,
    data: filteredData
  }
};

async function unregister () {
  return
}

module.exports = {
  register,
  unregister
}
