# Customize PeerTube playlists

* Customize sort order for playlists
* Possibility to remove deleted videos from playlists
* Possibility to hide private videos from playlists

# Remove deleted videos from playlists
* Upon enabling this feature a database purge will be done to cleanup all deleted videos from playlists.
* When this setting is enabled and a video is deleted a database purge will be done to ensure no playlists contains deleted videos.

# Sorting playlists

Makes it possible to customize order all playlists for a particular account. Supported sort options:

* Reverse order (latest added on top)
* Publish date (latest publish on top)

Playlists will be sorted upon:

* Adding a new element to a certain playlist.
* A video within the playlist is updated.

### Usage

1. Install plugin.
1. Go to plugin settings page.
1. Enter the account id for which the playlists should be reversed.
1. **Add a new element to every playlist that needs to be sorted.** 

### Limitations
* Sorting playlists only works for a single account as is, but merge requests are welcome.
